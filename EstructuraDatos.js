Ejercicio: Diseñar la estructura de datos de Twitter
-------------------------------------------------------------
Vamos a centrarnos en cómo representar un Tweet
Un tweet consta básicamente de:
- Texto, multimedia, fecha de creación
Tiene que tener información del usuario que escribió  el tweet: nombre, id, foto, descripción,...
Almacenará el número de retweets y favoritos que tiene.
Tendrá el listado de menciones (respuestas):
- Cada mención será otro tweet hecho por otro usuario
El tweet tendrá que saber si “él mismo” es una mención a otro tweet, o no.
- Si es una mención tendrá que saber a qué tweet está contestando
Los multimedia tendrán cierta información:
- tipo (imagen, video…), url, id, dimensiones


tweet{
    text: String,
    multimedia: [{
        tipo: enum,
        url: string,
        id: ObjectID,
        dimensiones: String
    }],
    fechaCreacion: Date,
    autor: ObjectID (REF a Usuarios),
    respuestaA: ObjectID (REF a tweet),
    RTs: array REF usuarios,
    FAVs: array REF usuarios,
    hashtag: array String
}

usuarios {
    nombre: String,
    mencion: String,
    multimedia: String,
    desc: string,
    tweets: array REF tweet,
    followers: array REF usuarios,
    following: array REF usuarios
}